import numpy as np
from utils import *


def create_sensor_transform(config):
    soft = np.array(config['model']['poisson']['soft'])
    hard = np.reshape(np.array(config['model']['poisson']['hard']), (-1, 1))
    poissonTransform = np.append(np.append(soft, hard, axis=1), np.array([[0, 0, 0, 1]]), axis=0)

    axesTransform = np.identity(4)
    for index, gain in enumerate(config['model']['sensor']['gains']):
        axesTransform[index, index] = gain
    for index, bias in enumerate(config['model']['sensor']['biases']):
        axesTransform[index, 3] = bias

    mountPhi, mountTheta, mountPsi = config['model']['mounting']
    mountRot = rotation_matrix(mountPhi, mountTheta, mountPsi, affine=True)

    # Induction vector is affected by deviation (coupled to body), then mounting error, then axes errors
    sensorTransform = np.matmul(axesTransform, np.matmul(mountRot, poissonTransform))

    return sensorTransform


def create_calibration_attitudes(config):
    rollCount = config['attitudes']['rollCount']
    pitchCount = config['attitudes']['pitchCount']
    yawCount = config['attitudes']['yawCount']

    # -180° to +180°
    rolls = np.reshape(np.linspace(-np.pi, np.pi, num=rollCount), (-1, 1))
    # -80° to +80°
    pitches = np.reshape(np.linspace(-(80 / 180) * np.pi, (80 / 180) * np.pi, num=pitchCount), (-1, 1))
    # -180° to +180°
    yaws = np.reshape(np.linspace(-np.pi, np.pi, num=yawCount), (-1, 1))

    rollsPitches = np.append(np.tile(rolls, (pitchCount, 1)), np.repeat(pitches, rollCount, axis=0), axis=1)

    """
    r1 p1 y1
    r2 p1 y1
    r1 p2 y1
    r2 p2 y1
    r1 p1 y2
    ...
    """
    return np.append(
        np.tile(rollsPitches, (yawCount, 1)),
        np.repeat(yaws, rollCount * pitchCount, axis=0),
        axis=1)


def create_measurements(attitudes, sensorTransform, config):
    magField = np.reshape(np.append(np.array(config['induction']), [1]), (-1, 1))  # append 1 to allow translations

    points = np.zeros((attitudes.shape[0], 3))
    for i, attitude in enumerate(attitudes):
        rot = rotation_matrix(attitude[0], attitude[1], attitude[2], affine=True)
        points[i] = np.matmul(sensorTransform, np.matmul(rot, magField))[:3, 0]  # strip the translating 1

    absoluteDeviation = np.linalg.norm(magField[:3, 0]) * config['error']['relativeDeviation']
    points = points + np.random.normal(loc=0.0, scale=absoluteDeviation, size=points.shape)
    # TODO: Implement random large error for some points (completely wrong value, bit flip etc.)

    return points
