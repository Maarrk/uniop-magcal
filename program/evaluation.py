import json
import numpy as np
import matplotlib.pyplot as plt
from model import *
from utils import *

# configFile = open('input/identity.json', 'r')
configFile = open('input/testcase.json', 'r')
configDict = json.load(configFile)

sensorTransform = create_sensor_transform(configDict)
calibrationTransform = np.genfromtxt('output/calibration_transform.csv', delimiter=',', skip_header=0)

inductionMagnitude = np.linalg.norm(np.array(configDict['induction']))
reference = spiral_sphere(1000) * inductionMagnitude
calibrated = np.zeros_like(reference)

for i, p in enumerate(reference):
    vec = np.reshape(np.concatenate((p, [1])), (-1, 1))
    calibrated[i, :] = np.matmul(calibrationTransform, np.matmul(sensorTransform, vec))[:3, 0]

delta = calibrated - reference / inductionMagnitude
distances = np.rad2deg(np.sqrt(np.sum(np.multiply(delta, delta), axis=1)))

n, bins, patches = plt.hist(distances, 100, density=True)

plt.xlabel('Wartość błędu [deg]')
plt.ylabel('Liczba wystąpień')
plt.title('Błąd kierunku wektora indukcji po kalibracji')
plt.axvline(np.mean(distances), color='red')
plt.text(np.mean(distances) * 1.02, np.max(n), 'Średni błąd', color='red')
plt.grid(True)
plt.show()

print('Średni błąd {:.5f}'.format(np.mean(distances)))

yaws = np.linspace(-np.pi*0.9, np.pi*0.9, num=100)
calibratedFlat = np.zeros((100, 3))
measuredFlat = np.zeros_like(calibratedFlat)
for i, y in enumerate(yaws):
    p = np.array((np.cos(y), np.sin(y), 0))
    vec = np.reshape(np.concatenate((p, [1])), (-1, 1))
    calibratedFlat[i, :] = np.matmul(calibrationTransform, np.matmul(sensorTransform, vec))[:3, 0]
    measuredFlat[i, :] = np.matmul(sensorTransform, vec)[:3, 0]

yaws = np.rad2deg(yaws)
calibratedYaw = np.rad2deg(np.arctan2(calibratedFlat[:, 1], calibratedFlat[:, 0]))
measuredYaw = np.rad2deg(np.arctan2(measuredFlat[:, 1], measuredFlat[:, 0]))

plt.plot(yaws, calibratedYaw, 'go')
plt.plot(yaws, measuredYaw, 'ro')
plt.ylabel('Kurs kompasowy [°]')
plt.xlabel('Kurs magnetyczny [°]')
plt.axis([-180, 180, -180, 200])
plt.axline((0, 0), (360, 360), color='darkblue', linestyle='--')
plt.show()

plt.plot(yaws, np.abs(calibratedYaw - yaws), 'go')
plt.plot(yaws, np.abs(measuredYaw - yaws), 'ro')
plt.ylabel('Błąd kursu [°]')
plt.xlabel('Kurs magnetyczny [°]')
plt.show()

print('Średni błąd w płaszczyźnie {}'.format(np.mean(np.abs(calibratedYaw - yaws))))
