import json
from model import *
from ellipsoid import *
from mounting import *

# configFile = open('input/identity.json', 'r')
configFile = open('input/testcase.json', 'r')
configDict = json.load(configFile)

sensorTransform = create_sensor_transform(configDict)

filenames = {
    'calibration': 'output/measurements_calibration.csv',
    'roll': 'output/measurements_roll.csv',
    'pitch': 'output/measurements_pitch.csv',
}


def write_points(filename, attitudes):
    with open(filename, 'w') as outFile:
        measurements = create_measurements(attitudes, sensorTransform, configDict)

        outFile.write('x,y,z\n')
        for m in measurements:
            outFile.write('{},{},{}\n'.format(m[0], m[1], m[2]))
        print('Written {} points to "{}"'.format(measurements.shape[0], filename))


calibrationAttitudes = create_calibration_attitudes(configDict)
write_points(filenames['calibration'], calibrationAttitudes)

count = configDict['attitudes']['alignmentCount']
rollAttitudes = np.zeros((count, 3))
rollAttitudes[:, 0] = np.linspace(-np.pi, np.pi, num=count)
write_points(filenames['roll'], rollAttitudes)

pitchAttitudes = np.zeros((count, 3))
pitchAttitudes[:, 1] = np.linspace(-np.pi, np.pi, num=count)  # this violates the pitch range conventions, but is fine?
write_points(filenames['pitch'], pitchAttitudes)

ellipsoidParams = fit_ellipsoid(filenames['calibration'])
np.set_printoptions(precision=5, suppress=True)
print('Ellipsoid:')
reference = np.concatenate((configDict['model']['sensor']['gains'], configDict['model']['sensor']['biases']))
print(reference)
print(ellipsoidParams)


def corrected(points):
    alpha_prime = np.repeat(1 / ellipsoidParams[np.newaxis, :3], points.shape[0], axis=0)
    beta_prime = np.repeat(-ellipsoidParams[np.newaxis, 3:6], points.shape[0], axis=0)
    return np.multiply(alpha_prime, points + beta_prime)


def write_calibrated(filename_out, filename_in):
    points = np.genfromtxt(filename_in, delimiter=',', skip_header=1)
    data = corrected(points)
    with open(filename_out, 'w') as outFile:
        outFile.write('x,y,z\n')
        for d in data:
            outFile.write('{},{},{}\n'.format(d[0], d[1], d[2]))
        print('Written {} points to "{}"'.format(data.shape[0], filename_out))


for key, fname in filenames.items():
    write_calibrated(fname.replace('measurements', 'calibrated'), fname)

u1 = rotation_axis('output/calibrated_roll.csv')
u2 = rotation_axis('output/calibrated_pitch.csv')
sensorOrientation = find_orientation(u1, u2)

offsetCorrection = np.identity(4)
offsetCorrection[:3, 3] = - ellipsoidParams[3:6]
gainCorrection = np.identity(4)
for i in range(3):
    gainCorrection[i, i] = 1 / ellipsoidParams[i]
mountCorrection = rotation_matrix(-sensorOrientation[0], -sensorOrientation[1], -sensorOrientation[2], affine=True)

calibrationTransform = np.matmul(mountCorrection, np.matmul(gainCorrection, offsetCorrection))
with open('output/calibration_transform.csv', 'w') as outFile:
    for row in calibrationTransform:
        outFile.write('{:.5f},{:.5f},{:.5f},{:.5f}\n'.format(row[0], row[1], row[2], row[3]))


def write_mounted(filename_out, filename_in):
    points = np.genfromtxt(filename_in, delimiter=',', skip_header=1)
    with open(filename_out, 'w') as outFile:
        outFile.write('x,y,z\n')
        for p in points:
            vec = np.reshape(np.concatenate((p, [1])), (-1, 1))
            d = np.matmul(calibrationTransform, vec).squeeze()
            outFile.write('{},{},{}\n'.format(d[0], d[1], d[2]))
        print('Written {} points to "{}"'.format(points.shape[0], filename_out))


for key, fname in filenames.items():
    write_mounted(fname.replace('measurements', 'mounted'), fname)
