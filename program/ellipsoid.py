import numpy as np


# https://github.com/aleksandrbazhin/ellipsoid_fit_python
def fit_ellipsoid(points_csv_path):
    points = np.genfromtxt(points_csv_path, delimiter=',', skip_header=1)

    x = points[:, 0]
    y = points[:, 1]
    z = points[:, 2]
    D = np.array([x * x + y * y - 2 * z * z,
                  x * x + z * z - 2 * y * y,
                  2 * x * y,
                  2 * x * z,
                  2 * y * z,
                  2 * x,
                  2 * y,
                  2 * z,
                  1 - 0 * x])
    d2 = np.array(x * x + y * y + z * z).T  # rhs for LLSQ
    u = np.linalg.solve(D.dot(D.T), D.dot(d2))
    a = np.array([u[0] + 1 * u[1] - 1])
    b = np.array([u[0] - 2 * u[1] - 1])
    c = np.array([u[1] - 2 * u[0] - 1])
    v = np.concatenate([a, b, c, u[2:]], axis=0).flatten()
    A = np.array([[v[0], v[3], v[4], v[6]],
                  [v[3], v[1], v[5], v[7]],
                  [v[4], v[5], v[2], v[8]],
                  [v[6], v[7], v[8], v[9]]])

    center = np.linalg.solve(- A[:3, :3], v[6:9])

    translation_matrix = np.eye(4)
    translation_matrix[3, :3] = center.T

    R = translation_matrix.dot(A).dot(translation_matrix.T)
    radii_oriented = np.sqrt(-R[3, 3] / R[:3, :3].diagonal())

    return np.concatenate((radii_oriented, center))
