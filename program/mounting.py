import numpy as np


def rotation_axis(points_csv_path):
    points = np.genfromtxt(points_csv_path, delimiter=',', skip_header=1)

    r = np.mean(points, axis=0)
    if np.linalg.norm(r) >= 0.5:
        return r / np.linalg.norm(r)

    # Fit a plane to points
    delta = points - r[np.newaxis, :]
    M = np.dot(delta.T, delta)  # moment of inertia
    return np.linalg.svd(M)[0][:, -1]  # Singular Value Decomposition of moment of inertia finds principal axis


def find_orientation(u1, u2):
    # flip inverted axes, assuming mounting error < 90°
    if u1[0] < 0:
        u1 = -u1
    if u2[1] < 0:
        u2 = -u2

    v = np.cross(u1, u2)
    v = v / np.linalg.norm(v)
    uAvg = (u1 + u2) / 2

    # perpendicular to each other, in the same plane as u1, u2
    u1p = uAvg + np.cross(uAvg, v)
    u2p = uAvg + np.cross(v, uAvg)

    u1p = u1p / np.linalg.norm(u1p)
    u2p = u2p / np.linalg.norm(u2p)

    psi_s = np.arctan2(u1p[1], u1p[0])
    theta_s = -np.arcsin(u1p[2])

    cp, sp = np.cos(-psi_s), np.sin(-psi_s)
    ct, st = np.cos(-theta_s), np.sin(-theta_s)
    Tp = np.array([
        [cp, -sp, 0],
        [sp, cp, 0],
        [0, 0, 1]
    ])
    Tt = np.array([
        [ct, 0, st],
        [0, 1, 0],
        [-st, 0, ct]
    ])
    phi_s = np.arcsin(np.matmul(Tt, np.matmul(Tp, u2p.T))[2])

    return np.array([phi_s, theta_s, psi_s])
