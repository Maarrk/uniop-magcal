import numpy as np


def rotation_matrix(phi, theta, psi, affine=False):
    cx, sx = np.cos(phi), np.sin(phi)
    cy, sy = np.cos(theta), np.sin(theta)
    cz, sz = np.cos(psi), np.sin(psi)

    rotX = np.array([
        [1, 0, 0],
        [0, cx, -sx],
        [0, sx, cx]
    ])
    rotY = np.array([
        [cy, 0, sy],
        [0, 1, 0],
        [-sy, 0, cy]
    ])
    rotZ = np.array([
        [cz, -sz, 0],
        [sz, cz, 0],
        [0, 0, 1]
    ])

    rot = np.identity(3)
    rot = np.matmul(rotX, rot)
    rot = np.matmul(rotY, rot)
    rot = np.matmul(rotZ, rot)

    if affine:
        return np.append(np.append(rot, np.array([[0], [0], [0]]), axis=1), np.array([[0, 0, 0, 1]]), axis=0)
    else:
        return rot


def spiral_sphere(numPoints):
    turnFraction = (1 + np.sqrt(5)) / 2
    points = np.zeros((numPoints, 3))
    for i in range(numPoints):
        phi = np.arccos(1 - 2 * (i + 0.5) / numPoints)
        theta = 2 * np.pi * turnFraction * i

        x = np.cos(theta) * np.sin(phi)
        y = np.sin(theta) * np.sin(phi)
        z = np.cos(phi)
        points[i, :] = (x, y, z)

    return points