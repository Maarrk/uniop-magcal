import bpy
import math

def point_cloud(ob_name, coords, edges=[], faces=[]):
    """Create point cloud object based on given coordinates and name.

    Keyword arguments:
    ob_name -- new object name
    coords -- float triplets eg: [(-1.0, 1.0, 0.0), (-1.0, -1.0, 0.0)]
    """

    me = bpy.data.meshes.new(ob_name + "Mesh")
    ob = bpy.data.objects.new(ob_name, me)
    me.from_pydata(coords, edges, faces)

    # ob.show_name = True
    me.update()
    return ob

root_path = 'D:/Marek/My-Repos/uniop-magcal/program/output/'
def add_points(filename, coll):
    coords = []
    with open(root_path + filename, 'r') as pointFile:
        pointFile.readline()  # strip header
        for line in pointFile:
            words = line.split(',')
            numbers = [float(w) for w in words]
            coords.append(numbers)

    pc = point_cloud(filename, coords)
    coll.objects.link(pc)

collection = bpy.data.collections.new('Measurements')
bpy.context.scene.collection.children.link(collection)
add_points('measurements_calibration.csv', collection)
add_points('measurements_roll.csv', collection)
add_points('measurements_pitch.csv', collection)

collection = bpy.data.collections.new('Calibrated')
bpy.context.scene.collection.children.link(collection)
add_points('calibrated_calibration.csv', collection)
add_points('calibrated_roll.csv', collection)
add_points('calibrated_pitch.csv', collection)

collection = bpy.data.collections.new('Mounted')
bpy.context.scene.collection.children.link(collection)
add_points('mounted_calibration.csv', collection)
add_points('mounted_roll.csv', collection)
add_points('mounted_pitch.csv', collection)
