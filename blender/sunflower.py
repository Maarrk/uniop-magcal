import bpy
import math

collection = bpy.data.collections.new('Points')
bpy.context.scene.collection.children.link(collection)

numPoints = 1000
turnFraction = (1 + math.sqrt(5)) / 2

coords = []
for i in range(numPoints):
    phi = math.acos(1 - 2*(i + 0.5)/numPoints)
    theta = 2 * math.pi * turnFraction * i

    x = math.cos(theta) * math.sin(phi)
    y = math.sin(theta) * math.sin(phi)
    z = math.cos(phi)
    coords.append((x, y, z))

def point_cloud(ob_name, coords, edges=[], faces=[]):
    """Create point cloud object based on given coordinates and name.

    Keyword arguments:
    ob_name -- new object name
    coords -- float triplets eg: [(-1.0, 1.0, 0.0), (-1.0, -1.0, 0.0)]
    """

    me = bpy.data.meshes.new(ob_name + "Mesh")
    ob = bpy.data.objects.new(ob_name, me)
    me.from_pydata(coords, edges, faces)

    # ob.show_name = True
    me.update()
    return ob

pc = point_cloud("PointCloud", coords)
collection.objects.link(pc)
