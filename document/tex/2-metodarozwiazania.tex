\newpage
\section{Metoda rozwiązania}
\label{sec:metoda}

\subsection{Założenia}
Aby zamodelować dewiację magnetyczną przyjęto model Poissona opisany równaniem \ref{eq:poisson}.
Zakłada się w nim że wektor pola magnetycznego $ H $, i zmierzone składowe $ h $ są powiązane równaniem liniowym.
Występujące współczynniki odpowiadają magnetyzacji przejściowej ($ K $, \emph{soft disturbance}) oraz trwałej ($ K $, \emph{hard disturbance}).

\begin{equation}
  \label{eq:poisson}
  \begin{aligned}
    \vec{H} &= \boldsymbol{K}\vec{h} + \vec{B} \\
    \\
    \begin{bmatrix}
      H_x \\
      H_y \\
      H_z
    \end{bmatrix}
    &=
    \begin{bmatrix}
      K_{11} & K_{12} & K_{13} \\
      K_{21} & K_{22} & K_{23} \\
      K_{31} & K_{32} & K_{33}
    \end{bmatrix}
    \begin{bmatrix}
      h_x \\ h_y \\ h_z
    \end{bmatrix}
    +
    \begin{bmatrix}
      b_x \\ b_y \\ b_z
    \end{bmatrix}
  \end{aligned}
\end{equation}

W literaturze są opisane różne metody obliczania parametrów modelu Poissona, m.~in. 12-punktowa metoda koziołkująca (\emph{12-point tumbling method}).
Szczególnie popularna w nawigacji morskiej i samolotowej jest metoda opisania dewiacji szeregiem Fouriera, jednak wymaga wykonania wielu pomiarów ze znanym poprawnym kursem magnetycznym i dotyczy przypadku dwuwymiarowego.
Określenie wszystkich dwunastu współczynników modelu Poissona jest możliwe za pomocą algorytmów regresji, natomiast byłoby dość złożone obliczeniowo, a współczynniki $ K $ leżące poza główną przekątną w rzeczywistych eksperymentach z reguły są bardzo małe w porównaniu do pozostałych.

Z powyższych powodów, stosowana jest hipoteza eliptyczna \cite{Camps2009} czyli uproszczenie modelu Poissona, w którym zakłada się że nieskalibrowane pomiary w wyniku obracania magnetometru rysują elipsę.
Wtedy, dla przypadku trójwymiarowego model \ref{eq:poisson} upraszcza się do postaci przedstawionej w równaniu \ref{eq:poissonelliptic}.

\begin{equation}
  \label{eq:poissonelliptic}
  \begin{aligned}
    \begin{bmatrix}
      H_x \\
      H_y \\
      H_z
    \end{bmatrix}
    &=
    \begin{bmatrix}
      K_{11} & 0 & 0 \\
      0 & K_{22} & 0 \\
      0 & 0 & K_{33}
    \end{bmatrix}
    \begin{bmatrix}
      h_x \\ h_y \\ h_z
    \end{bmatrix}
    +
    \begin{bmatrix}
      b_x \\ b_y \\ b_z
    \end{bmatrix}
  \end{aligned}
\end{equation}

Dodatkową zaletą zastosowania powyższego uproszczenia jest fakt, że po odwróceniu równania \ref{eq:poissonelliptic} aby otrzymać funkcję $ h(H) $ jest ono takiej samej postaci jak liniowy model czujnika magnetycznego opisany w \ref{eq:modelczujnika}.
Dzięki temu, wykonanie kalibracji magnetometru zamontowanego na statku powietrznym jednocześnie określa kalibrację czujnika, oraz uproszczoną poprawkę od dewiacji.

Wtedy problem kalibracji czujnika i obliczenia poprawki dewiacji sprowadza się do dopasowania elipsoidy do zbioru punktów pomiarowych, co można wykonać różnymi algorytmami regresyjnymi.
Znając elipsoidę na której rozłożone są punkty pomiarowe, przekształcić ją na jednostkową sferę o środku w początku układu współrzędnych.
Jest to powierzchnia którą w idealnym przypadku zakreślałby koniec stałego wektora indukcji magnetycznej w układzie współrzędnych czujnika wskutek obracania obiektu.

Mając taką kalibrację, można następnie przejść do orientowania układu osi pomiarowych czujnika względem układu współrzędnych związanego ze statkiem powietrznym.
W tym celu należy wykonać pełny obrót wokół jednej z osi układu współrzędnych s.~p., w wyniku czego koniec mierzonego wektora pola magnetycznego zakreśli w układzie współrzędnych okrąg, przez którego środek przebiega oś wokół której wykonywany był obrót.
W przypadku kiedy kąt pomiędzy osią obrotu a mierzonym wektorem jest duży (zbliżony do 90°), zamiast poszukiwać środka okręgu można znaleźć płaszczyznę na której położony jest ten okrąg, wtedy poszukiwana oś obrotu jest prostopadła do tej płaszczyzny.
Mając zdefiniowane kierunki i zwroty dwóch osi trójwymiarowego układu współrzędnych można jednoznacznie określić jego orientację.

\subsection{Wzory obliczeń}
Elipsoida dopasowywana do punktów pomiarowych jest opisana wzorem \ref{eq:elipsoida}.
Szukane parametry $ A, B, C $ będące półosiami elips zorientowanych równolegle do płaszczyzn układu współrzędnych odpowiadają wzmocnieniom poszczególnych osi czujnika oraz dewiacji przejściowej.
Parametry $ D, E, F $ opisujące środek elipsoidy wynikają z biasu czujnika oraz dewiacji trwałej.

\begin{equation}
  \label{eq:elipsoida}
  \frac{(x - D)^2}{A^2} + \frac{(y - E)^2}{B^2} + \frac{(z - F)^2}{C^2} = 1
\end{equation}

Do regresji zbioru punktów pomiarowych wykorzystano algorytm minimalizacji błędu kwadratowego, w którym funkcja błędu $ f $ jest sumą kwadratów względnej odległości pomiędzy danym punktem, a powierzchnią aproksymowanej elipsoidy.
Określenie ,,względ\-na od\-ległość'' wynika z faktu że wektor od środka elipsoidy do danego punktu jest przeskalowany przez promienie elipsoidy w odpowiednich kierunkach.
Opisana funkcja kosztu jest przedstawiona w równaniu \ref{eq:funkcjakosztu}, gdzie $ N $ oznacza liczbę punktów pomiarowych.

\begin{equation}
  \label{eq:funkcjakosztu}
  f = \sum_{i=1}^N \left| \frac{(x_i - D)^2}{A^2} + \frac{(y_i - E)^2}{B^2} + \frac{(z_i - F)^2}{C^2} - 1 \right|
\end{equation}

W części programu określającej orientację przy dwóch osiach $ \vec{u_1}, \vec{u_2} $, problem może być nadokreślony, jeśli kąt pomiędzy nimi nie jest równy 90°.
Aby rozwiązać ten problem, można wyznaczyć dwa nowe wektory $ \vec{u_1}', \vec{u_2}' $ w tej płaszczyźnie, obrócone o 45° od wektora średniego $ \vec{u}_{avg} $ wokół osi $ v $, przez dodanie odpowiedniego prostopadłego wektora o tej samej długości.
Konkretne operacje przedstawiono w równaniu \ref{eq:prostopadle}

\begin{equation}
  \label{eq:prostopadle}
  \begin{aligned}
    \vec{v} &= \frac{\vec{u_1} \times \vec{u_2}}{||\vec{u_1} \times \vec{u_2}||} \\
    \\
    \vec{u}_{avg} &= \frac{\vec{u_1} + \vec{u_2}}{2} \\
    \\
    \vec{u_1}' &= \vec{u}_{avg} + \vec{u}_{avg} \times \vec{v} \\
    \\
    \vec{u_2}' &= \vec{u}_{avg} + \vec{v} \times \vec{u}_{avg}
  \end{aligned}
\end{equation}