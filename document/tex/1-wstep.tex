\newpage
\section{Wstęp}

\subsection{Opis zagadnienia}
Tematem projektu jest opracowanie algorytmu i programu obliczeniowego służącego do autonomicznej kompensacji dewiacji dla magnetometru zamontowanego na statku powietrznym.
Określenie ,,autonomiczna kompensacja'' oznacza że niedostępne są często wykorzystywane zewnętrzne dane, przykładowo model pola magnetycznego Ziemi \cite{wmm2020}, lub wykonanie pomiarów znając kurs rzeczywisty (wartość kąta odchylenia) obiektu \cite{mag2004}.

\subsection{Założenia dotyczące modelu}
Na podstawie przeglądu literatury naukowej \cite{Zhi2009}, \cite{Papafotis2019}, \cite{Hu2019} oraz dokumentacji technicznej magnetometrów zamontowanych w bezzałogowych statkach powietrznych wykorzystywanych przez autora, przyjęto następujący model czujnika.
Trzyosiowy magnetometr omierzący składowe lokalnej wartości pola indukcji magnetycznej $ B $ rzutowane na kierunek osi pomiarowych.
Osie pomiarowe są do siebie prostopadłe i tworzą prawoskrętny układ współrzędnych $ X_s Y_s Z_s $ czujnika.
Każda ma liniową funkcję przejścia, charakteryzowaną równaniem \ref{eq:funkcjaprzejscia}.
Czujnik liniowy z biasem oraz wzmocnieniem wymagającym kalibracji jest często stosowanym modelem dla magnetometrów, w szczególności typu \emph{MEMS} montowanych na bezzałogowych statkach powietrznych.

\begin{equation}
  \label{eq:funkcjaprzejscia}
  V = \alpha \cdot B + \beta
\end{equation}

Kompletny model czujnika został przedstawiony w równaniu \ref{eq:modelczujnika}, gdzie $ B_s $ oznacza wektor indukcji magnetycznej w układzie współrzędnych czujnika, a $ \epsilon $ błąd pomiaru.

\begin{equation}
  \label{eq:modelczujnika}
  \begin{aligned}
    \vec{V} &= \boldsymbol{\alpha} \cdot \vec{B_s} + \vec{\beta} + \vec{\epsilon} \\
    \\
    \begin{bmatrix}
      V_x \\
      V_y \\
      V_z
    \end{bmatrix}
    &=
    \begin{bmatrix}
      \alpha_x & 0 & 0 \\
      0 & \alpha_y & 0 \\
      0 & 0 & \alpha_z
    \end{bmatrix}
    \begin{bmatrix}
      B_{sx} \\ B_{sy} \\ B_{sz}
    \end{bmatrix}
    +
    \begin{bmatrix}
      \beta_x \\ \beta_y \\ \beta_z
    \end{bmatrix}
    +
    \begin{bmatrix}
      \epsilon_x \\ \epsilon_y \\ \epsilon_z
    \end{bmatrix}
  \end{aligned}
\end{equation}

Algorytmy kalibracji magnetometrów wymagają zmiany mierzonego pola magnetycznego, co można osiągnąć poprzez zmianę położenia czujnika, lub zmianę otaczającego go pola zewnętrznymi przyrządami do indukcji odpowiednich poprawek \cite{Zikmund2014}.
Ponieważ wymagany jest autonomiczny algorytm, konieczne jest obracanie obiektu na którym zamocowany jest magnetometr.
Zakłada się że orientacja przestrzenna obiektu jest znana w odniesieniu do pewnej arbitralnej orientacji początkowej na podstawie zamontowanego na obiekcie układu giroskopów oraz przyspieszeniomierzy.
Przetwarzanie danych z czujników inercyjnych jest poza tematem tego projektu.

Ponieważ modelowany magnetometr ma służyć do nawigacji statku powietrznego, zakłada sie że mierzone jest pole magnetyczne Ziemi, które w skali wymiarów statku powietrznego można uznać za jednorodne.
W jednorodnym polu położenie czujnika można pominąć, dzięki czemu wartość wektora indukcji magnetycznej w układzie współrzędnych czujnika zależy tylko od jego orientacji.

Aby opisać orientację czujnika, wprowadzono dodatkowe ortogonalne prawoskrętne układy współrzędnych.
Układ nawigacyjny $ X_g Y_g Z_g $ lokalnego horyzontu (\emph{north east down}), w którym oś $ Z_g $ skierowana jest w dół, zgodnie z wektorem siły grawitacji, a oś $ X_g $ na północ, przez co oś $ Y_g $ ma zwrot wschodni. Zakłada się że w tym układzie wektor indukcji pola magnetycznego ziemi pozostaje stały.
Układ statku powietrznego $ X_b Y_b Z_b $ (\emph{body-fixed}), w którym oś $ X_b $ skierowana jest do przodu obiektu, $ Y_b $ w prawo, a $ Z_b $ w dół.
Relacja pomiędzy układami $ b $ oraz $ g $ jest charakteryzowana przez kąty orientacji przestrzennej: przechylenie $ \phi $, pochylenie $ \theta $ oraz odchylenie $ \psi $.
Kolejność wykonywania operacji obrotu wokół poszczególnych osi jednoznacznie definiuje równanie \ref{eq:katyeulera}.
Aby modelować błąd montażu czujnika na pokładzie statku powietrznego, także układ współrzędnych $ s $ w którym dokonywany jest pomiar może być obrócony względem układu $ b $ o tak samo zdefiniowane, niewielkie kąty.
W trakcie wykonywania kalibracji, kąty orientacji przestrzennej statku powietrznego względem układu nawigacyjnego zmieniają się, natomiast kąty orientacji czujnika względem s.p. pozostają stałe.

\begin{equation}
  \label{eq:katyeulera}
  \begin{aligned}
    \vec{x_b} &= \boldsymbol{T_g^b} \cdot \vec{x_g} \\
    \\
    \vec{x_s} &= \boldsymbol{T_b^s} \cdot \vec{x_b} = \boldsymbol{T_b^s} \cdot \boldsymbol{T_g^b} \cdot \vec{x_g} \\
    \\
    \boldsymbol{T_g^b} = \boldsymbol{T_b^s} &=
    \left[
    \begin{smallmatrix}
      \cos(\psi)\cos(\theta) & \sin(\psi)\cos(\theta) & -\sin(\theta) \\
      \cos(\psi)\sin(\theta)\sin(\phi) - \sin(\psi)\cos(\phi) & \sin(\psi)\sin(\theta)\sin(\phi) - \cos(\psi)\cos(\phi) & \cos(\theta)\sin(\phi) \\
      \cos(\psi)\sin(\theta)\cos(\phi) - \sin(\psi)\sin(\phi) & \sin(\psi)\sin(\theta)\cos(\phi) - \cos(\psi)\sin(\phi) & \cos(\theta)\cos(\phi)
    \end{smallmatrix}
    \right]
  \end{aligned}
\end{equation}

\subsection{Cele do osiągnięcia w projekcie}
Zadania wykonywane przez przygotowywany program można podzielić na trzy główne grupy:
\begin{itemize}
  \item Symulacja czujnika i wytworzenie zbioru danych pomiarowych
  \item Kalibracja czujnika
  \item Ewaluacja jakości kalibracji
\end{itemize}

Po zrealizowaniu podstawowej implementacji algorytmu, część symulacyjna jest rozszerzana o modele kolejnych błędów.
Oprócz błędów przesunięcia i skali pomiarów opisanych w równaniu \ref{eq:funkcjaprzejscia}, następnie dodano szum zmierzonej wartości i błąd zamocowania.

Końcowa ewaluacja polega na porównaniu błędu wskazywanego kursu przed wykonaniem kalibracji oraz po zastosowaniu obliczonych poprawek.
